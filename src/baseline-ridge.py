# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression, Lasso, Ridge, ElasticNet, SGDRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.metrics import mean_absolute_error, mean_squared_error

# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory

train = pd.read_csv('../input/train_V2.csv')
test = pd.read_csv('../input/test_V2.csv')
sub = pd.read_csv('../input/sample_submission_V2.csv')

# get_dummy for categoricial data
categorical_feature = ['matchType']
train = pd.get_dummies(train, columns = categorical_feature)
test = pd.get_dummies(test, columns = categorical_feature)

train.loc[train.winPlacePerc.isnull(),'winPlacePerc'] = -1

# normalize data
train_x = train.drop(columns = ['Id','groupId','matchId','winPlacePerc'])
test_x = test.drop(columns = ['Id','groupId','matchId'])
scaler = StandardScaler()
scaler.fit(train_x)
scaler.transform(train_x)
scaler.transform(test_x)
# scaler.transform(test_x)
train_y = train.winPlacePerc

# RidgeRegression
ridge_model = Ridge(alpha=0.05, fit_intercept=True, normalize=False, copy_X=True, max_iter=None, tol=0.001, solver='auto', random_state=777)
ridge_model.fit(train_x.values, train_y.values)
test_y = ridge_model.predict(test_x)

sub['winPlacePerc']=test_y
sub.to_csv('submission_panda_express.csv', index = False)