# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression, Lasso, Ridge, ElasticNet, SGDRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.metrics import mean_absolute_error, mean_squared_error
# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory
import os
import gc

# feature engineering
def feature_engineering(is_train):
    y = None
    if is_train:
        df = pd.read_csv('../input/train_V2.csv') 
    else:
        df = pd.read_csv('../input/test_V2.csv')
        
    df = df[df['maxPlace'] > 1] # ??
    df['total_distance'] = df['rideDistance'] + df['walkDistance'] + df['swimDistance']
        
    target = 'winPlacePerc'
    features = list(df.columns)
    features.remove('Id')
    features.remove('matchId')
    features.remove('groupId')
    features.remove('matchType')
    if is_train:
        features.remove(target)
        df.loc[df.winPlacePerc.isnull(),'winPlacePerc'] = -1
        y = np.array(df.groupby(['matchId','groupId'])[target].agg('mean'), dtype=np.float64)
    # matchType = pd.get_dummies(df['matchType'])
    # df = df.join(matchType)
    
    # get mean/mean_rank feature 
    agg = df.groupby(['matchId','groupId'])[features].agg('mean')
    agg_rank = agg.groupby('matchId')[features].rank(pct=True).reset_index()  
    
    if is_train:
        df_out = agg.reset_index()[['matchId','groupId']]
    else:
        df_out = df[['matchId','groupId']] # for submission
    
    df_out = df_out.merge(agg.reset_index(), suffixes=["", ""], how='left', on=['matchId', 'groupId'])
    df_out = df_out.merge(agg_rank, suffixes=["_mean", "_mean_rank"], how='left', on=['matchId', 'groupId'])
    
    # get sum/sum_rank feature
    agg = df.groupby(['matchId','groupId'])[features].agg('sum')
    agg_rank = agg.groupby('matchId')[features].rank(pct=True).reset_index()
    df_out = df_out.merge(agg.reset_index(), suffixes=["",""],how='left',on=['matchId','groupId'])
    df_out = df_out.merge(agg_rank, suffixes=["_sum", "_sum_rank"],how='left',on=['matchId','groupId'])
    
    # get max/max_rank feature
    agg = df.groupby(['matchId','groupId'])[features].agg('max')
    agg_rank = agg.groupby('matchId')[features].rank(pct=True).reset_index()
    df_out = df_out.merge(agg.reset_index(), suffixes=["", ""], how='left', on=['matchId', 'groupId'])
    df_out = df_out.merge(agg_rank, suffixes=["_max", "_max_rank"], how='left', on=['matchId', 'groupId'])

    # get min/min_rank feature
    agg = df.groupby(['matchId','groupId'])[features].agg('min')
    agg_rank = agg.groupby('matchId')[features].rank(pct=True).reset_index()
    df_out = df_out.merge(agg.reset_index(), suffixes=["", ""], how='left', on=['matchId', 'groupId'])
    df_out = df_out.merge(agg_rank, suffixes=["_min", "_min_rank"], how='left', on=['matchId', 'groupId'])
    
    # get match mean and size feature
    agg = df.groupby(['matchId','groupId']).size().reset_index(name='group_size')
    df_out = df_out.merge(agg, how='left', on=['matchId', 'groupId'])
        
    agg = df.groupby(['matchId'])[features].agg('mean').reset_index()
    df_out = df_out.merge(agg, suffixes=["", "_match_mean"], how='left', on=['matchId'])
 
    agg = df.groupby(['matchId']).size().reset_index(name='match_size')
    df_out = df_out.merge(agg, how='left', on=['matchId'])

    # get delta feature// to be done 
    
    del df, agg, agg_rank
    gc.collect()
    
    return df_out,y
    

train_x, train_y = feature_engineering(True)
test_x,_ = feature_engineering(False)

# normalize data
train_x = train_x.drop(columns = ['groupId','matchId'])
test_x = test_x.drop(columns = ['groupId','matchId'])
scaler = StandardScaler()
scaler.fit(train_x)
scaler.transform(train_x)
scaler.transform(test_x)

# training
# split data
seed = 7651234
# x_train, x_valid, y_train, y_valid = train_test_split(train_x, train_y, test_size = 0.2, random_state=seed)

# RidgeRegression
ridge_model = Ridge(alpha=0.05, fit_intercept=True, normalize=False, copy_X=True, max_iter=None, tol=0.001, solver='auto', random_state=seed)
ridge_model.fit(train_x, train_y)
test_y = ridge_model.predict(test_x)

        
# Any results you write to the current directory are saved as output.
sub = pd.read_csv('../input/sample_submission_V2.csv')
sub['winPlacePerc'] = test_y
sub.to_csv('submission_ridge_panda_express_1102.csv', index = False)