# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression, Lasso, Ridge, ElasticNet, SGDRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.metrics import mean_absolute_error, mean_squared_error
import lightgbm as lgb

# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory

train = pd.read_csv('../input/train_V2.csv')
test = pd.read_csv('../input/test_V2.csv')
sub = pd.read_csv('../input/sample_submission_V2.csv')

# get_dummy for categoricial data
categorical_feature = ['matchType']
train = pd.get_dummies(train, columns = categorical_feature)
test = pd.get_dummies(test, columns = categorical_feature)

train.loc[train.winPlacePerc.isnull(),'winPlacePerc'] = -1

# normalize data
train_x = train.drop(columns = ['Id','groupId','matchId','winPlacePerc'])
test_x = test.drop(columns = ['Id','groupId','matchId'])
scaler = StandardScaler()
scaler.fit(train_x)
scaler.transform(train_x)
scaler.transform(test_x)
train_y = train.winPlacePerc

# split data
seed = 7651234
x_train, x_valid, y_train, y_valid = train_test_split(train_x.values, train_y.values, test_size = 0.1, random_state=seed)


# baseline_lightbgm
# create dataset for lightgbm
lgb_train = lgb.Dataset(x_train, y_train)
lgb_eval = lgb.Dataset(x_valid, y_valid, reference=lgb_train)

# specify your configurations as a dict
params = {
    'task': 'train',
    'boosting_type': 'gbdt',
    'objective': 'regression',
    'metric': 'mae',
    'num_leaves': 31,
    'learning_rate': 0.05,
    'feature_fraction': 0.9,
    'bagging_fraction': 0.8,
    'bagging_freq': 5,
    'verbose': 0
}


# train
gbm = lgb.train(params,
                lgb_train,
                num_boost_round=2000,
                valid_sets=[lgb_train, lgb_eval],
                valid_names=['train', 'valid'],
                early_stopping_rounds=10)

# predict
test_y = gbm.predict(test_x, num_iteration=gbm.best_iteration)

sub['winPlacePerc']=test_y
sub.to_csv('submission_lightgbm_panda_express.csv', index = False)
