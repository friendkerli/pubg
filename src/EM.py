import pandas as pd
from sklearn.linear_model import LinearRegression, Lasso, Ridge, ElasticNet, SGDRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.metrics import mean_absolute_error, mean_squared_error

import matplotlib.pyplot as plt

train = pd.read_csv('../data/train_V2.csv')
test = pd.read_csv('../data/test_V2.csv')
sub = pd.read_csv('../data/sample_submission_V2.csv')


# get_dummy for categoricial data
categorical_feature = ['matchType']
train = pd.get_dummies(train, columns = categorical_feature)
test = pd.get_dummies(test, columns = categorical_feature)

print(train[:10])


# normalize data
train_x = train.drop(columns = ['Id','groupId','matchId','winPlacePerc'])
test_x = test.drop(columns = ['Id','groupId','matchId'])

plt.matshow(train_x.corr())